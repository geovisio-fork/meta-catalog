# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

This project is based on [eoAPI](https://github.com/developmentseed/eoAPI), this file only lists changes since the fork.

## [Unreleased]

### Changed
- STAC API moved to `api/metacatalog`

### Deleted
- Removed `vector` and `raster` Docker components to only keep the STAC stack.


[Unreleased]: https://gitlab.com/geovisio/meta-catalog/-/compare/main...1ad2481fba907d5dd2dd466518cfde646e56811e
