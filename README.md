# ![GeoVisio](https://gitlab.com/geovisio/api/-/raw/develop/images/logo_full.png)

**GeoVisio** is a complete solution for storing and **serving your own 📍📷 geolocated pictures** (like [StreetView](https://www.google.com/streetview/) / [Mapillary](https://mapillary.com/)).

<!--➡️ __Give it a try__ at [panoramax.ign.fr](https://panoramax.ign.fr/) or [geovisio.fr](https://geovisio.fr/viewer) ! -->

# 📖 GeoVisio Meta-Catalog

This repository only contains the **meta-catalog**, ie the search engine to find pictures accross all [Panoramax](https://panoramax.fr/) STAC instances.

## Features

- A **web API** to search pictures collections (code in `./api`)

  - Compatible with [SpatioTemporal Asset Catalog](https://stacspec.org/) (abbreviated as STAC) and [OGC WFS 3](https://github.com/opengeospatial/WFS_FES) specifications
  - Also able to serve vector tiles to easily display the data on a map.

- An **API crawler** to download and import STAC instances metadata inside the database (code in `./harvester`)

## Install & run

The classic way to run this project is by using Docker compose:

```bash
git clone https://gitlab.com/geovisio/meta-catalog.git
cd meta-catalog/
docker compose up --build
```

Then, the meta-catalog is accessible at [localhost:9000](http://localhost:9000/) (without any data for the moment).

You can also run each service without docker, as is explained in each services documentation.

If at some point you're lost or need help, you can contact us through [issues](https://gitlab.com/geovisio/meta-catalog/-/issues) or by [email](mailto:panieravide@riseup.net).

### Database & migrations

Each service will need an accessible postgis database.

The database will be provided and its schema updated if you use the docker compose approach, else you need to provide it.

Note: in all the following examples, the database will be on `localhost:5432`, named as `panoramax` and accessed with `username:password` (thus the connection string will be `postgresql://username:password@localhost:5432/panoramax`).

The database schema needs to be updated. To do this manually:

```bash
cd /migrations

python -m pip install --upgrade virtualenv
virtualenv .venv
source .venv/bin/activate

pip install -e .
yoyo apply --database postgresql+psycopg://username:password@localhost:5432/panoramax
```

### Loading data

Loading data in the meta catalog is an explicit step for the moment.

Note: In the future, we'll try to make it more automated.

The `harvester` directory contains to code needed to harvest the data from several instances into the meta-catalog.

The `harvester` depend on a simple CSV file to describe to instances to crawl.

The CSV should have the column:

- `id`: identifier of the instance
- `api_url`: Root url of the stac catalog. Note: for geovisio instances, don't forget the trailing `/api`

You can check the [config file of french public panoramax instances](./panoramax-fr-instances.csv) for an example.

#### Docker compose

To use the docker compose, you need to have a running metacatalog

```bash
docker compose up
```

Or

```bash
docker compose up -d
```

If you want it to run in the background.

Then you need to run

```bash
INSTANCES_FILE=<CSV_FILE> docker compose -f docker-compose.yml -f docker-compose-harvester.yml run --rm harvester
```

Where `INSTANCES_FILE` is the path to a csv file containing data about the instances to crawl.

#### Install standalone Harvester

You can also just run the python code directly:

```bash
cd harvester
python -m pip install --upgrade virtualenv
virtualenv .venv
source .venv/bin/activate

pip install -e .

stac-harvester --db "postgresql://username:password@localhost:5432/panoramax" --instances-files ../panoramax-ign.csv --target-hostname=https://panoramax.fr/api --collections-limit=5
```

This will import 5 collections from the panoramax.ign.fr instance into your database.

Note: the parameter `--target-hostname` is important since all STAC links from the crawled API will be replaced with this hostname. This should be the public hostname of your meta-catalog API.

All CLI parameters can be listed with `stac-harvester --help`.

#### Service systemd

If you want to run a [systemd](https://wikipedia.org/wiki/Systemd) service to craw automatically, you can check the [detailed documentation section](./docs/systemd/readme.md#harvester)..

### Install standalone API

The API is written in [Rust](https://www.rust-lang.org/fr).

To build it without docker, you need to install it. The best is to follow the official documentation, but if you want a quick way to do this:

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

#### Build and run

```bash
cd api
cargo run -- --db-url=postgres://username:password@localhost:5432/panoramax
```

Note: on production, you'll want to use a release build

```bash
cd api
cargo run --release
```

#### Service systemd

If you want to run the API as a [systemd](https://wikipedia.org/wiki/Systemd) service, you can use and adapt the [example configuration file](./docs/systemd/panoramax.service).

## Development

#### API

You can compile without running with:

```bash
cargo build
```

Then you can run `./target/debug/panoramax-api --db-url=postgres://username:password@localhost:5432/panoramax`.

If you also want to quickly know if the compilation is ok you can do

```bash
cargo check
```

For development purpose, you might want hot reload to run build on file changes. To do this, install [cargo-watch](https://github.com/watchexec/cargo-watch):

```bash
cd api
cargo install cargo-watch
```

then

```bash
RUST_LOG=info PORT=9001 DB_URL=postgres://username:password@localhost:5432/panoramax cargo watch -x run
```

```bash
cd api
cargo install cargo-watch
```

##### Matomo

Usage statistics can be sent to a configured matomo instance by providing it's url/site-id when running the API.

The environment variables for this are:

- `MATOMO_URL`
- `MATOMO_SITE_ID`

#### Tests

To run the tests:

```
cd api
cargo test
```

### Harvester

#### Tests

To run the tests, install the `[dev]` requirements in your virtualenv (cf [the installation section](#install-standalone-harvester)), then run pytest:

```bash
pip install -e .[dev]
pytest
```

Note: By default, the tests use a [Docker Compose](https://docs.docker.com/compose/) environment (using `./docker-compose.yml` and `./harvester/tests/docker-compose-geovisio.yml`) to set-up a temporary stack. You can also spawn the docker compose on you own, then run the tests using the external docker compose used. This can speed up greatly the tests:

```bash
docker compose -f docker-compose.yml -f harvester/tests/docker-compose-geovisio.yml up -d
# wait a bit for all services to be started

# run pytest telling it where to find all services
pytest --external-geovisio_instance_1-url=http://localhost:5101/api --external-geovisio_instance_2-url=http://localhost:5102/api  --external-api-url http://localhost:9000 --external-db-url postgresql://username:password@localhost:5439/panoramax --external-secrets-url http://localhost:5100 -vv -s
```

## Contributing

Pull requests are welcome. For major changes, please open an [issue](https://gitlab.com/geovisio/meta-catalog/-/issues) first to discuss what you would like to change.

More specific documentation on development and contribution is available in [contributing doc](./CONTRIBUTING.md).

## 🤗 Special thanks

![Sponsors](https://gitlab.com/geovisio/api/-/raw/develop/images/sponsors.png)

GeoVisio was made possible thanks to a group of ✨ **amazing** people ✨ :

- **[GéoVélo](https://geovelo.fr/)** team, for 💶 funding initial development and for 🔍 testing/improving software
- **[Carto Cité](https://cartocite.fr/)** team (in particular Antoine Riche), for 💶 funding improvements on viewer (map browser, flat pictures support)
- **[La Fabrique des Géocommuns (IGN)](https://www.ign.fr/institut/la-fabrique-des-geocommuns-incubateur-de-communs-lign)** for offering long-term support and funding the [Panoramax](https://panoramax.fr/) initiative and core team (Camille Salou, Mathilde Ferrey, Christian Quest, Antoine Desbordes, Jean Andreani, Adrien Pavie)
- Many _many_ **wonderful people** who worked on various parts of GeoVisio or core dependencies we use : 🧙 Stéphane Péneau, 🎚 Albin Calais & Cyrille Giquello, 📷 [Damien Sorel](https://www.strangeplanet.fr/), Pascal Rhod, Nick Whitelegg...
- **[Adrien Pavie](https://pavie.info/)**, for ⚙️ initial development of GeoVisio
- And you all ✨ **GeoVisio users** for making this project useful !

## ⚖️ License

This project is based on [Development Seed](http://developmentseed.org)'s [eoAPI](https://github.com/developmentseed/eoAPI) solution. It was forked to meet Panoramax needs.

Copyright (c) GeoVisio team 2023, Copyright (c) 2021 Development Seed, [released under MIT license](https://gitlab.com/geovisio/meta-catalog/-/blob/develop/LICENSE).
