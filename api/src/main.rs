use actix_cors::Cors;
use actix_web::{http::header, middleware, web, App, HttpServer};
use clap::Parser;
use sqlx::postgres::PgPoolOptions;
use std::error::Error;
use std::net::Ipv4Addr;
use utils::matomo::MatomoConfig;

mod model;
mod routes;
mod utils;

#[derive(Parser, Clone)]
#[command(author, version, about, long_about = None)]
pub struct Config {
    #[clap(long, env)]
    pub db_url: String,

    #[clap(long, short, env, default_value = "9000")]
    pub port: u16,

    #[clap(long, env)]
    pub matomo_url: Option<String>,
    #[clap(long, env)]
    pub matomo_site_id: Option<u16>,
}

#[actix_web::main]
async fn main() -> Result<(), impl Error> {
    env_logger::init();

    let config = Config::parse();

    let db = PgPoolOptions::new()
        .min_connections(2)
        .connect(&config.db_url)
        .await
        .expect("impossible to connect to db");

    let matomo_config = MatomoConfig {
        url: config.matomo_url,
        site_id: config.matomo_site_id.map(|s| s.to_string()),
    };

    HttpServer::new(move || {
        let cors = Cors::default()
            .allow_any_origin()
            .allowed_methods(vec!["GET"])
            .allowed_headers(vec![
                header::CONTENT_TYPE,
                header::AUTHORIZATION,
                header::ACCEPT,
            ]);

        // This factory closure is called on each worker thread independently.
        App::new()
            .app_data(web::Data::new(db.clone()))
            .app_data(web::Data::new(matomo_config.clone()))
            .wrap(middleware::Logger::default())
            .wrap(middleware::NormalizePath::new(
                middleware::TrailingSlash::Trim,
            ))
            .wrap(cors)
            .configure(routes::mount)
    })
    .bind((Ipv4Addr::UNSPECIFIED, config.port))?
    .run()
    .await
}
