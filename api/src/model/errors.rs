use actix_web::{http::StatusCode, HttpResponse, ResponseError};
use serde::Serialize;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum APIError {
    #[error("Invalid query")]
    InvalidQuery(#[from] sqlx::Error),
    #[error("Invalid parameter: {0}")]
    InvalidParameter(String),
    #[error("{obj_type} not found")]
    NotFound {
        id: uuid::Uuid,
        obj_type: &'static str,
    },
}

#[derive(Serialize, Debug)]
struct JsonError {
    status: u16,
    message: String,
    #[serde(skip)]
    detail: Option<String>,
}

impl ResponseError for APIError {
    fn error_response(&self) -> HttpResponse {
        let status = self.status_code();
        log::info!("error: {:?}", self);

        HttpResponse::build(status).json(JsonError::from(self))
    }

    fn status_code(&self) -> StatusCode {
        match *self {
            APIError::InvalidQuery(ref sql_error) => match sql_error {
                sqlx::Error::PoolTimedOut | sqlx::Error::PoolClosed => {
                    log::error!("impossible to connect to database: {}", self);
                    StatusCode::INTERNAL_SERVER_ERROR
                }
                _ => StatusCode::INTERNAL_SERVER_ERROR,
            },
            APIError::NotFound { id: _, obj_type: _ } => StatusCode::NOT_FOUND,
            APIError::InvalidParameter(_) => StatusCode::BAD_REQUEST,
        }
    }
}

impl JsonError {
    fn new(err: &APIError) -> Self {
        JsonError {
            status: err.status_code().as_u16(),
            message: err.to_string(),
            detail: None,
        }
    }

    fn detail(mut self, d: String) -> Self {
        self.detail = Some(d);
        self
    }
}

impl From<&APIError> for JsonError {
    fn from(e: &APIError) -> JsonError {
        match e {
            APIError::InvalidQuery(sql_error) => match sql_error {
                sqlx::Error::RowNotFound => JsonError {
                    status: e.status_code().as_u16(),
                    message: "Impossible to find object".to_string(),
                    detail: None,
                },
                _ => JsonError::new(e).detail(sql_error.to_string()),
            },

            APIError::NotFound { id: _, obj_type: _ } => JsonError::new(e),
            APIError::InvalidParameter(detail) => JsonError::new(e).detail(detail.clone()),
        }
    }
}
