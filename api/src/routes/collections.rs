use crate::model::APIError;
use actix_web::{get, web, Result};
use serde_json::Value;
use sqlx::postgres::{PgPool, Postgres};
use sqlx::QueryBuilder;

#[derive(serde::Deserialize)]
pub struct CollectionsQuery {
    pub limit: Option<i16>,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct Collections {
    pub collections: Vec<Value>,
    pub links: Vec<stac::Link>,
}

#[derive(sqlx::FromRow)]
struct Collection {
    content: Value,
}

/// Get list of collections.
#[utoipa::path(
    responses(
        (status = 200, description = "List collections", body = ref("https://api.stacspec.org/v1.0.0/collections/openapi.yaml#/components/schemas/collections"))
    ),
    tag = "Collections"
)]
#[get("/collections")]
pub async fn get_collections(
    db: web::Data<PgPool>,
    params: web::Query<CollectionsQuery>,
) -> Result<web::Json<Collections>, APIError> {
    let mut query = QueryBuilder::<Postgres>::new("SELECT content FROM collections");
    if let Some(l) = params.limit {
        query.push(" LIMIT ").push_bind(l);
    }

    let col: Vec<Value> = query
        .build_query_as::<Collection>()
        .fetch_all(db.as_ref())
        .await?
        .into_iter()
        .map(|c| c.content)
        .collect();

    Ok(web::Json(Collections {
        collections: col,
        links: vec![],
    }))
}

/// Get a specific collection.
#[utoipa::path(
    responses(
        (status = 200, description = "List collections", body = ref("https://api.stacspec.org/v1.0.0/collections/openapi.yaml#/components/schemas/collections"))
    ),
    tag = "Collections"
)]
#[get("/collections/{id}")]
pub async fn get_collection(
    db: web::Data<PgPool>,
    id: web::Path<uuid::Uuid>,
) -> Result<web::Json<Value>, APIError> {
    let col: Collection =
        sqlx::query_as::<_, Collection>("SELECT content FROM collections WHERE id = $1")
            .bind(*id)
            .fetch_optional(db.as_ref())
            .await?
            .ok_or(APIError::NotFound {
                id: *id,
                obj_type: stac::COLLECTION_TYPE,
            })?;

    Ok(web::Json(col.content))
}
