use actix_web::web;
use utoipa::OpenApi;
use utoipa_redoc::{Redoc, Servable};
use utoipa_swagger_ui::SwaggerUi;

#[derive(OpenApi)]
#[openapi(
    paths(
        super::collections::get_collections,
        super::collections::get_collection,
        super::items::get_collection_items,
        super::items::get_collection_item,
        super::items::get_search_items,
        super::items::post_search_items,
        super::map::get_tile,
        super::root::root
    ),
    components(schemas(crate::routes::items::SearchParameters))
)]
struct ApiDoc;

pub fn mount_docs(conf: &mut web::ServiceConfig) {
    let openapi = ApiDoc::openapi();

    conf.service(Redoc::with_url("/redoc", openapi.clone()))
        .service(SwaggerUi::new("/swagger-ui/{_:.*}").url("/openapi.json", openapi.clone()));
}
