use actix_web::HttpResponse;
use actix_web::{get, web};

use crate::utils::matomo::{matomo_html, MatomoConfig};

const VIEWER: &str = r#"
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<link rel="shortcut icon" href="/favicon.ico" />
	<link rel="icon" type="image/svg+xml" href="/static/img/favicon.svg" />
	<title>GeoVisio</title>
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/geovisio@2.3.1/build/index.css" />
	<style>
		#viewer {
			position: absolute;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
		}
	</style>
</head>
<body>
	<div id="viewer">
		<noscript>You need to enable JavaScript to run this app.</noscript>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/geovisio@2.3.1/build/index.js"></script>
    {matomo}
	<script>
		var instance = new GeoVisio.default(
			"viewer",
			"/api",
			{ map: { startWide: true } }
		);
	</script>
</body>
</html>
"#;

// Note: to update the geovisio-viewer version, just update it's version in the VIEWER variable (don't forget the css).

#[get("/")]
pub async fn index(config: web::Data<MatomoConfig>) -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .append_header(("X-Hdr", "sample"))
        .body(VIEWER.replace("{matomo}", &matomo_html(&config)))
}
