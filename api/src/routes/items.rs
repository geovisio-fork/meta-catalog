use crate::model::APIError;
use crate::utils::serde_utils::deserialize_as_stac_opt_list;
use actix_web::{get, post, web, Result};
use serde_json::Value;
use sqlx::postgres::{PgPool, Postgres};
use sqlx::QueryBuilder;
use utoipa::ToSchema;

#[derive(serde::Serialize, serde::Deserialize)]
pub struct Items {
    pub features: Vec<Value>,
    pub links: Vec<stac::Link>,
}

#[derive(sqlx::FromRow)]
struct Item {
    content: Value,
}

/// List all items of a collection
#[utoipa::path(
    responses(
        (status = 200, description = "List all items of a collection", body = ref("https://api.stacspec.org/v1.0.0/collections/openapi.yaml#/components/schemas/collections"))
    ),
    tag = "Items"
)]
#[get("/collections/{id}/items")]
pub async fn get_collection_items(
    db: web::Data<PgPool>,
    id: web::Path<uuid::Uuid>,
) -> Result<web::Json<Items>, APIError> {
    let items: Vec<_> =
        sqlx::query_as::<_, Item>("SELECT content FROM items WHERE collection_id = $1")
            .bind(*id)
            .fetch_all(db.as_ref())
            .await?
            .into_iter()
            .map(|i| i.content)
            .collect();

    Ok(web::Json(Items {
        features: items,
        links: vec![],
    }))
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct GetItem {
    pub collection_id: uuid::Uuid,
    pub id: uuid::Uuid,
}

/// Get a specific item of a collection
#[utoipa::path(
    responses(
        (status = 200, description = "Get a specific item of a collection", body = ref("https://api.stacspec.org/v1.0.0/collections/openapi.yaml#/components/schemas/collections"))
    ),
    tag = "Items"
)]
#[get("/collections/{collection_id}/items/{id}")]
pub async fn get_collection_item(
    db: web::Data<PgPool>,
    params: web::Path<GetItem>,
) -> Result<web::Json<Value>, APIError> {
    let item =
        sqlx::query_as::<_, Item>("SELECT content FROM items WHERE id = $1 AND collection_id = $2")
            .bind(params.id)
            .bind(params.collection_id)
            .fetch_optional(db.as_ref())
            .await?
            .ok_or(APIError::NotFound {
                id: params.id,
                obj_type: stac::ITEM_TYPE,
            })?;

    Ok(web::Json(item.content))
}

#[derive(Debug, serde::Deserialize, ToSchema)]
pub struct SearchParameters {
    /// Limit the number of results
    limit: Option<i16>,

    /// Search for items with those IDs
    #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
    ids: Option<Vec<uuid::Uuid>>,

    /// Search for items belonging to those collections
    #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
    collection_ids: Option<Vec<uuid::Uuid>>,

    /// Search for items inside this bounding box
    #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
    pub bbox: Option<Vec<f32>>,
    // TODO: datetime, intersect
}

/// Search for some items
#[utoipa::path(
    responses(
        (status = 200, description = "Search for some items", body = ref("https://api.stacspec.org/v1.0.0/collections/openapi.yaml#/components/schemas/collections"))
    ),
    tag = "Items",
)]
#[get("/search")]
pub async fn get_search_items(
    db: web::Data<PgPool>,
    params: web::Query<SearchParameters>,
) -> Result<web::Json<Items>, APIError> {
    search(db, params.into_inner()).await
}

#[utoipa::path(
    responses(
        (status = 200, description = "Search for some items", body = ref("https://api.stacspec.org/v1.0.0/collections/openapi.yaml#/components/schemas/collections"))
    ),
    tag = "Items",
)]
#[post("/search")]
pub async fn post_search_items(
    db: web::Data<PgPool>,
    params: web::Json<SearchParameters>,
) -> Result<web::Json<Items>, APIError> {
    search(db, params.into_inner()).await
}

async fn search(
    db: web::Data<PgPool>,
    params: SearchParameters,
) -> Result<web::Json<Items>, APIError> {
    let mut query = QueryBuilder::<Postgres>::new("SELECT content FROM items WHERE TRUE");
    if let Some(col_id) = &params.collection_ids {
        query
            .push(" AND collection_id = ANY(")
            .push_bind(col_id.as_slice())
            .push(")");
    }
    if let Some(ids) = &params.ids {
        query
            .push(" AND id = ANY(")
            .push_bind(ids.as_slice())
            .push(")");
    }
    if let Some(bbox) = &params.bbox {
        if bbox.len() != 4 {
            return Err(APIError::InvalidParameter(
                "Invalid bbox, the boundbing box should be an array with 4 values".to_string(),
            ));
        }
        query
            .push(" AND geom && ST_MakeEnvelope(")
            .push_bind(bbox[0])
            .push(", ")
            .push_bind(bbox[1])
            .push(", ")
            .push_bind(bbox[2])
            .push(", ")
            .push_bind(bbox[3])
            .push(", ")
            .push("4326)");
    }
    if let Some(l) = params.limit {
        query.push(" LIMIT ").push_bind(l);
    }

    let items: Vec<_> = query
        .build_query_as::<Item>()
        .fetch_all(db.as_ref())
        .await?
        .into_iter()
        .map(|i| i.content)
        .collect();

    Ok(web::Json(Items {
        features: items,
        links: vec![],
    }))
}
