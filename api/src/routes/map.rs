use crate::model::APIError;
use actix_web::{get, web, HttpResponse, Result};
use serde::Deserialize;
use sqlx::postgres::PgPool;
use sqlx::Row;

pub type Tile = Vec<u8>;

#[derive(Deserialize)]
pub struct TileRequest {
    z: i32,
    x: i32,
    y: i32,
}

/// Get a MapBox vector tile containing the collections and the items in the tile's geometry
#[utoipa::path(
    responses(
        (status = 200, description = "Get a MapBox vector tile", body = ref(""))
    ),
    tag = "Items"
)]
#[get("/map/{z}/{x}/{y}.mvt")]
pub async fn get_tile(
    db: web::Data<PgPool>,
    r: web::Path<TileRequest>,
) -> Result<HttpResponse, APIError> {
    let query = tile_query(r.as_ref());

    let pg_tile = sqlx::query(query)
        .bind(r.z)
        .bind(r.x)
        .bind(r.y)
        .fetch_one(db.as_ref())
        .await?;
    let raw_tile: Tile = pg_tile.get(0);
    if raw_tile.is_empty() {
        Ok(HttpResponse::NoContent()
            .content_type("application/vnd.mapbox-vector-tile")
            .finish())
    } else {
        Ok(HttpResponse::Ok()
            .content_type("application/vnd.mapbox-vector-tile")
            .body(pg_tile.get::<Tile, _>(0)))
    }
}

fn tile_query(request: &TileRequest) -> &'static str {
    if request.z >= 13i32 {
        r#"
SELECT mvtsequences.mvt || mvtpictures.mvt
FROM (
    SELECT ST_AsMVT(mvtgeomseqs.*, 'sequences') AS mvt
    FROM (
        SELECT
            ST_AsMVTGeom(ST_Transform(computed_geom, 3857), ST_TileEnvelope($1, $2, $3)) AS geom,
            id,
            computed_model AS model, computed_picture_type AS type, computed_capture_date AS date
        FROM collections
        WHERE computed_geom && ST_Transform(ST_TileEnvelope($1, $2, $3), 4326)
    ) mvtgeomseqs
) mvtsequences,
(
SELECT ST_AsMVT(mvtgeompics.*, 'pictures') AS mvt
FROM (
    SELECT
        ST_AsMVTGeom(ST_Transform(i.geom, 3857), ST_TileEnvelope($1, $2, $3)) AS geom,
        i.id, i.datetime,
        i.content #>> '{properties,view:azimuth}' as heading,
        i.collection_id AS sequences,
        i.picture_type AS type,
        TRIM(CONCAT(i.content #>> '{properties,pers:interior_orientation,camera_manufacturer}', 
                    ' ', 
                    i.content #>> '{properties,pers:interior_orientation,camera_model}')) AS model
    FROM items i
    WHERE i.geom && ST_Transform(ST_TileEnvelope($1, $2, $3), 4326)
    ) mvtgeompics
) mvtpictures
"#
    } else if request.z >= 7 {
        r#"
SELECT ST_AsMVT(mvtsequences.*, 'sequences') AS mvt
FROM (
    SELECT
        ST_AsMVTGeom(ST_Transform(computed_geom, 3857), ST_TileEnvelope($1, $2, $3)) AS geom,
        id,
        computed_model AS model, computed_picture_type AS type, computed_capture_date AS date
    FROM collections
    WHERE computed_geom && ST_Transform(ST_TileEnvelope($1, $2, $3), 4326)
) mvtsequences
"#
    } else {
        r#"
    SELECT ST_AsMVT(mvtsequences.*, 'sequences') AS mvt
    FROM (
        SELECT
        ST_AsMVTGeom(
            ST_Transform(geom, 3857),
            ST_TileEnvelope($1, $2, $3)
        ) AS geom,
        id
        FROM (
            SELECT ST_Simplify(computed_geom, 0.01) AS geom, id
            FROM collections
            WHERE computed_geom && ST_Transform(ST_TileEnvelope($1, $2, $3), 4326)
        ) s
        WHERE geom IS NOT NULL
    ) mvtsequences
    "#
    }
}
