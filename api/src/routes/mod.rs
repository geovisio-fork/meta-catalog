use actix_web::web;

mod collections;
mod doc;
mod index;
mod items;
mod map;
mod pictures;
mod root;

pub fn mount(conf: &mut web::ServiceConfig) {
    let api_scope = web::scope("/api")
        .service(root::root)
        .service(collections::get_collections)
        .service(collections::get_collection)
        .service(items::get_collection_items)
        .service(items::get_collection_item)
        .service(items::post_search_items)
        .service(items::get_search_items)
        .service(pictures::get_picture_asset)
        .service(map::get_tile);

    conf.configure(doc::mount_docs)
        .service(index::index)
        .service(api_scope);
}
