use crate::model::APIError;
use actix_web::{get, web, Result};
use sqlx::postgres::PgPool;

fn validate_asset(asset: &str) -> Result<&str, APIError> {
    match asset {
        "hd" | "sd" | "thumb" => Ok(asset),
        _ => Err(APIError::InvalidParameter(format!(
            "{} is not a valid asset, should be either hd, sd or thumb",
            asset
        ))),
    }
}

#[derive(sqlx::FromRow)]
pub struct DBPic {
    pub href: String,
}

/// Redirect to the picture file with its ID
#[utoipa::path(
    responses(
        (status = 308, description = "Redirect to a picture file")
    ),
    tag = "Items"
)]
#[get("/pictures/{id}/{asset}.jpg")]
pub async fn get_picture_asset(
    db: web::Data<PgPool>,
    qs: web::Path<(uuid::Uuid, String)>,
) -> Result<web::Redirect, APIError> {
    let (id, asset) = qs.as_ref();
    validate_asset(asset)?;

    let db_pic = sqlx::query_as::<_, DBPic>(&format!(
        "SELECT content->'assets'->'{asset}'->>'href' AS href FROM items WHERE id = $1"
    ))
    .bind(id)
    .fetch_optional(db.as_ref())
    .await?
    .ok_or(APIError::NotFound {
        id: id.to_owned(),
        obj_type: stac::ITEM_TYPE,
    })?;

    Ok(web::Redirect::to(db_pic.href).permanent())
}
