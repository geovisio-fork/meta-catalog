use crate::{model::APIError, utils::link};
use actix_web::{get, web, HttpRequest, Result};
use chrono::{DateTime, Utc};
use serde::Serialize;
use sqlx::PgPool;
use stac::{Catalog, Extent, SpatialExtent, TemporalExtent};
use stac_api::{Conformance, Root};

#[derive(Serialize)]
pub struct RootCatalog {
    #[serde(flatten)]
    root: Root,

    extent: Extent,
}

#[derive(sqlx::FromRow)]
struct MetaQuery {
    st_xmin: Option<f64>,
    st_ymin: Option<f64>,
    st_xmax: Option<f64>,
    st_ymax: Option<f64>,
    min: Option<DateTime<Utc>>,
    max: Option<DateTime<Utc>>,
}

/// Root of Panoramax
#[utoipa::path(
    responses(
        (status = 200, description = "root of panoramax api", body = ref("https://api.stacspec.org/v1.0.0/core/openapi.yaml#components/schemas/landingPage"))
    ),
    tag = "Metadata"
)]
#[get("")]
pub async fn root(
    db: web::Data<PgPool>,
    req: HttpRequest,
) -> Result<web::Json<RootCatalog>, APIError> {
    let res: MetaQuery = sqlx::query_as(
        r#"
    SELECT
    ST_XMin(ST_EstimatedExtent('items', 'geom')),
    ST_YMin(ST_EstimatedExtent('items', 'geom')),
    ST_XMax(ST_EstimatedExtent('items', 'geom')),
    ST_YMax(ST_EstimatedExtent('items', 'geom')),
    MIN(datetime), MAX(datetime)
FROM items
"#,
    )
    .fetch_one(db.as_ref())
    .await?;

    let mut extent = Extent::default();
    extent.spatial = SpatialExtent {
        bbox: vec![vec![
            res.st_xmin.unwrap_or(-180.0),
            res.st_ymin.unwrap_or(-90.0),
            res.st_xmax.unwrap_or(180.0),
            res.st_ymax.unwrap_or(90.0),
        ]],
    };
    extent.temporal = TemporalExtent {
        interval: vec![[
            res.min.map(|d| d.to_rfc3339()),
            res.max.map(|d| d.to_rfc3339()),
        ]],
    };

    let version = stac::STAC_VERSION;
    let mut catalog = Catalog::new(
        "panoramax",
        "This catalog list all geolocated pictures available in this Panoramax instance",
    );
    catalog.links = vec![
        link::from_service(&req, "root", "root").json(),
        link::from_path(&req, "/openapi.json", "service-desc").json(),
        link::from_path(&req, "/swagger-ui/", "service-doc"),
        link::from_service(&req, "get_collections", "child").json(),
        link::from_service(&req, "get_collections", "data").json(),
        link::from_service(&req, "get_search_items", "search").geojson(),
        link::from_path(&req, "/api/map/{z}/{x}/{y}.mvt", "xyz")
            .title("Pictures and sequences vector tiles".to_string())
            .r#type("application/vnd.mapbox-vector-tile".to_string()),
    ];

    // Build the root (landing page) endpoint.
    let root = RootCatalog {
        root: Root {
            catalog: catalog,
            conformance: Conformance {
                // TODO : should the conformance be computed from the harvested geovisio instance?
                conforms_to: vec![
                    stac_api::CORE_URI.to_string(),
                    "http://www.opengis.net/spec/ogcapi-features-1/1.0/conf/core".to_string(),
                    "http://www.opengis.net/spec/ogcapi-features-1/1.0/conf/geojson".to_string(),
                    format!("https://api.stacspec.org/v{version}/browseable"),
                    format!("https://api.stacspec.org/v{version}/collections"),
                    format!("https://api.stacspec.org/v{version}/ogcapi-features"),
                    format!("https://api.stacspec.org/v{version}/item-search"),
                ],
            },
        },
        extent: extent,
    };

    Ok(web::Json(root))
}
