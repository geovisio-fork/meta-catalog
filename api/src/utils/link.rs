use actix_web::HttpRequest;
use stac::Link;

pub fn from_service(req: &HttpRequest, service_name: &str, rel: &str) -> Link {
    Link::new(
        req.url_for_static(service_name)
            .expect(&format!("impossible to find service {}", service_name)),
        rel,
    )
}

pub fn from_path(req: &HttpRequest, path: &str, rel: &str) -> Link {
    let conn = req.connection_info();
    let base = format!("{}://{}", conn.scheme(), conn.host());
    Link::new(format!("{base}{path}"), rel)
}
