use serde::de::{self, SeqAccess};
use std::fmt;

use crate::model;

pub trait ParseFromString {
    fn parse_from_string(p: &str) -> Result<Self, model::APIError>
    where
        Self: std::marker::Sized;
}

impl ParseFromString for String {
    fn parse_from_string(p: &str) -> Result<Self, model::APIError> {
        Ok(p.to_string())
    }
}

impl ParseFromString for uuid::Uuid {
    fn parse_from_string(p: &str) -> Result<Self, model::APIError> {
        uuid::Uuid::parse_str(p)
            .map_err(|e| model::APIError::InvalidParameter(format!("{p} is not a valid uuid: {e}")))
    }
}

impl ParseFromString for f32 {
    fn parse_from_string(p: &str) -> Result<Self, model::APIError> {
        p.parse::<f32>()
            .map_err(|_e| model::APIError::InvalidParameter(format!("{p} is not a valid float")))
    }
}

/// Deserialize a List from either a list or a string with comma separated values
/// This makes it possible to handle STAC list parameters as:
/// * Either a real list (like in json)
/// * Either a comma separated string (like in query string)
pub fn deserialize_as_stac_opt_list<'de, D, I>(deserializer: D) -> Result<Option<Vec<I>>, D::Error>
where
    D: de::Deserializer<'de>,
    I: de::DeserializeOwned + ParseFromString,
{
    struct StringVecVisitor<I>(std::marker::PhantomData<I>);

    impl<'de, I> de::Visitor<'de> for StringVecVisitor<I>
    where
        I: de::DeserializeOwned + ParseFromString,
    {
        type Value = Option<Vec<I>>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("a string containing a coma separated list")
        }

        fn visit_none<E>(self) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            Ok(None)
        }

        fn visit_seq<V>(self, mut seq: V) -> Result<Self::Value, V::Error>
        where
            V: SeqAccess<'de>,
        {
            let mut values = Vec::new();
            while let Some(elt) = seq.next_element()? {
                values.push(elt);
            }
            Ok(Some(values))
        }

        fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            if v.is_empty() {
                return Ok(None);
            }
            let mut values = Vec::new();
            for id in v.split(",") {
                let id = I::parse_from_string(id).map_err(de::Error::custom)?;
                //let id = I::deserialize(id.into_deserializer())?;
                values.push(id);
            }
            Ok(Some(values))
        }
    }

    deserializer.deserialize_any(StringVecVisitor(std::marker::PhantomData::<I>))
}

#[cfg(test)]
mod test {
    use super::deserialize_as_stac_opt_list;
    use serde_json;

    #[derive(Debug, serde::Deserialize, PartialEq)]
    pub struct Params {
        #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
        ids: Option<Vec<String>>,
    }

    #[test]
    fn test_classic_json() {
        let j = r#"{"ids": ["bob", "bobette"]}"#;
        let parsed_params: Params = serde_json::from_str(j).expect("should be valid json");
        assert_eq!(
            parsed_params,
            Params {
                ids: Some(vec!["bob".to_string(), "bobette".to_string()])
            }
        );
    }
    #[test]
    fn test_empty_json() {
        let parsed_params: Params = serde_json::from_str("{}").expect("should be valid json");
        assert_eq!(parsed_params, Params { ids: None });
    }

    #[test]
    fn test_comma_separated() {
        let query = actix_web::web::Query::<Params>::from_query("ids=bob,bobette")
            .expect("should be valid query");
        assert_eq!(
            query.into_inner(),
            Params {
                ids: Some(vec!["bob".to_string(), "bobette".to_string()])
            }
        );
    }
    #[test]
    fn test_comma_separated_float() {
        #[derive(Debug, serde::Deserialize, PartialEq)]
        pub struct FloatParams {
            #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
            ids: Option<Vec<f32>>,
        }
        let query = actix_web::web::Query::<FloatParams>::from_query("ids=1.45,-543.5")
            .expect("should be valid query");
        assert_eq!(
            query.into_inner(),
            FloatParams {
                ids: Some(vec![1.45, -543.5])
            }
        );
    }

    #[test]
    fn test_invalid_float() {
        #[derive(Debug, serde::Deserialize, PartialEq)]
        pub struct FloatParams {
            #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
            ids: Option<Vec<f32>>,
        }
        let query = actix_web::web::Query::<FloatParams>::from_query("ids=pouet,plop");
        assert!(query.is_err());
        match query {
            Ok(_) => panic!("this should be an error"),
            Err(e) => assert_eq!(
                e.to_string(),
                "Query deserialize error: Invalid parameter: pouet is not a valid float".to_owned()
            ),
        }
    }
}
