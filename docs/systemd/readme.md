# Using system.d services

## API

Copy the file `panoramax.service` in `/usr/lib/systemd/system/`.

Reload the daemon:

```bash
 systemctl daemon-reload
```

Start the service:

```bash
service panoramax start
```

To get the status of the service:

```bash
service panoramax status
```

To get the service logs:

```bash
journalctl -u panoramax
```

## Harvester

2 files:

- the harvest service
- the timer around it

Copy the files `harvest-instance.service` and `harvest-instance.timer` in `/usr/lib/systemd/system/`.

Change the values you need to change:

- name of the instance, if you have several instance (and change also the name of the files)
- path to configuration file for the harvested instance
- frequency of the harvest

Reload the daemon:

```bash
systemctl daemon-reload
```

Start the timer:

```bash
systemctl start harvest-instance.timer
```
