FROM python:3.11-alpine as builder
# Multi stage build, first image is used to install dependencies

WORKDIR /srv/harvester
COPY pyproject.toml .

RUN python -m venv --copies /app/venv \
    && . /app/venv/bin/activate \
    && pip install .

FROM python:3.11-alpine
# Real image use preinstalled dependencies, and just copy files
WORKDIR /app
ENV VIRTUAL_ENV=/app/venv \
    PATH="/app/venv/bin:$PATH"

COPY --from=builder /app/venv ${VIRTUAL_ENV}

COPY . .
RUN pip install --no-deps .

CMD stac-harvester
