from harvester.model import Instance, Config
import harvester
from psycopg_pool import AsyncConnectionPool
from psycopg import AsyncConnection, AsyncCursor
import orjson
from uuid import UUID
from psycopg import sql
from typing import Dict, Any, List, Optional, Set
import logging
from dataclasses import dataclass
from tqdm import tqdm
from datetime import timedelta
from time import perf_counter
import aiohttp
import asyncio
from yarl import URL

USER_AGENT = f"stac-harvester_{harvester.__version__}"


@dataclass
class Harvest:
    id: UUID
    instance: Instance
    instance_id: UUID


async def get_collections(pool: AsyncConnectionPool, harvest: Harvest):
    """
    Crawl all collections from an instance, using the `next` link to get the next pages
    """
    async with aiohttp.ClientSession() as session:
        params = ""
        if harvest.instance.has_differential_updates:
            last_harvest_dt = await _get_last_harvest_dt(pool, harvest)
            if last_harvest_dt:
                formated_dt = last_harvest_dt.isoformat().replace("+00:00", "Z")
                params = f"?filter=status IN ('deleted','ready') AND updated > '{formated_dt}'"
        url = URL(f"{harvest.instance.root_url}/collections{params}")
        next_query: Optional[str] = str(url)
        while next_query:
            async with session.get(next_query, headers={"User-Agent": USER_AGENT}) as response:
                response.raise_for_status()
                collections = await response.json(encoding="utf-8")
                for c in collections["collections"]:
                    yield c

                next_query = next(
                    (l["href"] for l in collections["links"] if l["rel"] == "next"),
                    None,
                )


async def _get_last_harvest_dt(pool: AsyncConnectionPool, harvest: Harvest):
    async with pool.connection() as conn, conn.cursor() as cur:
        await cur.execute(
            """SELECT start
    FROM harvests 
    WHERE 
        instance_id = %(instance)s
    AND id != %(current_harvest)s
    ORDER BY start DESC
    LIMIT 1""",
            {"instance": harvest.instance_id, "current_harvest": harvest.id},
        )
        r = await cur.fetchone()

        return r[0] if r else None


async def get_items(collection_url: str):
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{collection_url}/items", headers={"User-Agent": USER_AGENT}) as response:
            response.raise_for_status()
            items = await response.json(encoding="utf-8")
            return items["features"]


async def _delete_collection(
    cur: AsyncCursor,
    collection: Dict[str, Any],
):
    logging.info(f"Deleting collection {collection['id']}")
    await cur.execute("DELETE FROM collections WHERE id = %(id)s;", {"id": collection["id"]})


async def _import_collection(
    cur: AsyncCursor,
    collection: Dict[str, Any],
    harvest: Harvest,
    config: Config,
):
    col_id = collection["id"]
    logging.debug(f"Loading collection {col_id}")

    collection_url = next(l["href"] for l in collection["links"] if l["rel"] == "self")
    _update_links(collection, instance=harvest.instance, new_hostname=config.host_name)

    await cur.execute(
        "SELECT true FROM collections WHERE id = %(id)s",
        {
            "id": col_id,
        },
    )
    existing = True if await cur.fetchone() else False

    await cur.execute(
        """
        INSERT INTO collections (content, instance_id)
        VALUES (%(col)s, %(instance)s)
        ON CONFLICT (id) DO
        UPDATE SET content=EXCLUDED.content;
        """,
        {
            "col": orjson.dumps(collection).decode(),
            "instance": harvest.instance_id,
        },
    )

    status = "updated" if existing else "created"
    await cur.execute(
        """
        INSERT INTO collection_harvest (collection_id, harvest_id, status)
        VALUES (%(col)s, %(harvest)s, %(status)s); -- TODO Handle update/delete
        """,
        {"col": col_id, "harvest": harvest.id, "status": status},
    )

    all_items = await get_items(collection_url)
    # creating a temporary table, only visible for this transaction, inited with the same schema as the `items` table.
    await cur.execute("CREATE TEMP TABLE items_ingest_temp ON COMMIT DROP AS SELECT * FROM items LIMIT 0;")
    async with cur.copy(
        """
            COPY items_ingest_temp
            (content, datetime)
            FROM stdin;
            """,
    ) as copy:
        for item in all_items:
            _update_links(item, harvest.instance, config.host_name)
            await copy.write_row(
                (
                    orjson.dumps(item).decode(),
                    item.get("properties", {})["datetime"],
                ),
            )

    await cur.execute(sql.SQL("DELETE FROM items WHERE collection_id = %(col)s"), params={"col": col_id})
    await cur.execute(
        sql.SQL(
            """
            INSERT INTO items (content, datetime) SELECT content, datetime FROM items_ingest_temp;
            """,
        )
    )
    # compute the collection fields that needs all the collection's item
    await cur.execute("SELECT compute_collections_fields(%(id)s)", {"id": col_id})


async def sync_collection(
    pool: AsyncConnectionPool,
    collection: Dict[str, Any],
    harvest: Harvest,
    config: Config,
):
    async with pool.connection() as conn:
        async with conn.cursor() as cur:
            async with conn.transaction():
                if collection.get("geovisio:status") == "deleted":
                    await _delete_collection(cur, collection)
                else:
                    await _import_collection(cur, collection, harvest, config)


def _update_links(item: Dict[str, Any], instance: Instance, new_hostname: str):
    for l in item.get("links", []):
        l["href"] = l["href"].replace(instance.root_url, new_hostname)


async def init_harvest(conn: AsyncConnection, instance: Instance) -> Harvest:
    async with conn.cursor() as cur:
        async with conn.transaction():
            instance_id = await (
                await cur.execute(
                    "INSERT INTO instances (name, url) VALUES (%(name)s, %(url)s) ON CONFLICT (name) DO UPDATE SET name=EXCLUDED.name RETURNING ID;",
                    {"name": instance.name, "url": instance.root_url},
                )
            ).fetchone()
            assert instance_id
            harvest_id = await (
                await cur.execute(
                    "INSERT INTO harvests (instance_id) VALUES (%(id)s) RETURNING id;",
                    {"id": instance_id[0]},
                )
            ).fetchone()
            assert harvest_id

            return Harvest(id=harvest_id[0], instance=instance, instance_id=instance_id[0])


async def cleanup_after_harvest(db: AsyncConnectionPool, harvest: Harvest):
    """Cleanup after harvest. For non differential updates, it means deleting collection that are not anymore in the harvested instance"""
    if harvest.instance.has_differential_updates:
        return  # nothing to do, deleted collections have beeen handled during import

    async with db.connection() as conn:
        async with conn.cursor() as cur:
            r = await cur.execute(
                """WITH
last_harvested_collections AS (
    SELECT * from collection_harvest WHERE harvest_id = %(harvest_id)s
)
, deleted_collections AS (
        SELECT id from collections WHERE id not IN (SELECT collection_id from last_harvested_collections) AND instance_id = %(instance_id)s
)
, cleanup_old_collection_harvest AS (
    -- also cleanup collection_harvest as it is useless to keep the old ones for non differential updates
    DELETE FROM collection_harvest WHERE harvest_id NOT IN (SELECT harvest_id from last_harvested_collections) AND collection_id IN (SELECT collection_id from last_harvested_collections)
)
DELETE FROM collections WHERE id IN (select id from deleted_collections);""",
                {"harvest_id": harvest.id, "instance_id": harvest.instance_id},
            )
            logging.info(f"🧹 {r.rowcount} collections deleted because they have been deleted from their instance since last harvest")

        # After a full harvest, we do a vacuum analyze since it will be needed to get good estimates for the API
        await conn.execute("VACUUM ANALYZE collections;")
        await conn.execute("VACUUM ANALYZE items;")


class Harvester:
    def __init__(self, cnx_pool) -> None:
        self.db: AsyncConnectionPool = cnx_pool

    async def harvest(self, instance: Instance, config: Config):
        async with self.db.connection() as conn:
            harvest = await init_harvest(conn, instance)
            logging.info(
                f"🚀 Starting {'⚡ differential' if instance.has_differential_updates else '🌀 full'} harvest {harvest.id} of instance {harvest.instance.name} ({harvest.instance.root_url})"
            )

        all_col = get_collections(self.db, harvest)
        with tqdm(
            desc="⚙️ Task to import collections",
            disable=config.raw_log,
            unit=" tasks",
        ) as pb_task_launched, tqdm(
            desc="✅ Collections imported",
            disable=config.raw_log,
            unit=" col",
        ) as pb_imported:
            ongoing_tasks: Set[asyncio.Task] = set()

            start = perf_counter()
            collection_idx = 0
            async for c in all_col:
                if config.collections_limit and collection_idx >= config.collections_limit:
                    logging.info("Reaching limit of collections to import, stopping harvest")
                    break

                logging.debug(f"asking to import {c['id']}")
                if len(ongoing_tasks) >= config.nb_concurrent_collections:
                    # Note: to limit the number of concurent task to run, I did not find better than this home made solution
                    # Wait for some import to finish before adding a new one
                    _done, ongoing_tasks = await asyncio.wait(ongoing_tasks, return_when=asyncio.FIRST_COMPLETED)
                task = asyncio.ensure_future(sync_collection(self.db, c, harvest, config))

                # when the task is finished, we update the progress bar
                task.add_done_callback(lambda _t: pb_imported.update(1))
                ongoing_tasks.add(task)

                pb_task_launched.update(1)
                collection_idx += 1

            # Wait for the remaining downloads to finish
            if ongoing_tasks:
                await asyncio.wait(ongoing_tasks)

        await cleanup_after_harvest(self.db, harvest)

        logging.info(f"🎉 All collections imported in {timedelta(seconds=perf_counter()-start)}")


def harvest_instances(db: str, instances: List[Instance], config: Config):
    async def _harvest_all():
        from psycopg_pool import AsyncConnectionPool

        async with AsyncConnectionPool(db, open=False, min_size=2, max_size=30, kwargs={"autocommit": True}) as pool:
            await pool.open()
            harvester = Harvester(cnx_pool=pool)
            for i in instances:
                await harvester.harvest(i, config)

    asyncio.run(_harvest_all())
