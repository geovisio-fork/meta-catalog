from dataclasses import dataclass
from typing import Optional


@dataclass
class Instance:
    name: str
    root_url: str

    has_differential_updates: bool = False  # in the futur this capability should be retreived from the API


@dataclass
class Config:
    host_name: str
    collections_limit: Optional[int] = None
    raw_log: bool = False
    nb_concurrent_collections: int = 1
