import requests
from .conftest import harvest, import_collection_to_geovisio, ExternalServicesUrl, FIXTURE_DIR, cleanup_geovisio_instance
import pytest
import pystac
from harvester import Instance


@pytest.fixture(scope="module", autouse=True)
def cleanup_geovisio_instance_1(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers):
    """Delete all collections from geovisio instance 1"""
    cleanup_geovisio_instance(external_services.geovisio_instance_1_url, geovision_instance_1_auth_headers)


@pytest.fixture(scope="module", autouse=True)
def import_pic_in_geovisio_instance_1(external_services: ExternalServicesUrl, cleanup_geovisio_instance_1) -> str:
    """import a collection into geovisio"""
    return import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[FIXTURE_DIR / "col1" / "e1.jpg", FIXTURE_DIR / "col1" / "e2.jpg", FIXTURE_DIR / "col1" / "e3.jpg"],
    )


# all tests in this module can know that the geovisio_instance_1 has been harvested
# Note: no test in this module should load data, If a test needs to change the data loaded, it should be in another module
# This way, the tests in this module can be idempotent and be run in a random order or in parallele
@pytest.fixture(scope="module", autouse=True)
def harvest_instance_1(external_services, import_pic_in_geovisio_instance_1):
    harvest(
        instance=Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url),
        external_services=external_services,
    )


def test_root_endpoint(external_services):
    r = requests.get(f"{external_services.api_url}/api")
    assert r.status_code == 200
    data = r.json()

    # we remove the `extent` field for later, not to bother about the coordinate precision
    extent = data.pop("extent")

    assert extent["temporal"] == {"interval": [["2022-10-19T07:56:34+00:00", "2022-10-19T07:56:38+00:00"]]}

    assert [round(float(f), 6) for f in extent["spatial"]["bbox"][0]] == [-1.684547, 48.155064, -1.684468, 48.155075]

    assert data == {
        "conformsTo": [
            "https://api.stacspec.org/v1.0.0/core",
            "http://www.opengis.net/spec/ogcapi-features-1/1.0/conf/core",
            "http://www.opengis.net/spec/ogcapi-features-1/1.0/conf/geojson",
            "https://api.stacspec.org/v1.0.0/browseable",
            "https://api.stacspec.org/v1.0.0/collections",
            "https://api.stacspec.org/v1.0.0/ogcapi-features",
            "https://api.stacspec.org/v1.0.0/item-search",
        ],
        "description": "This catalog list all geolocated pictures available in this Panoramax instance",
        "id": "panoramax",
        "links": [
            {"href": f"{external_services.api_url}/api", "rel": "root", "type": "application/json"},
            {"href": f"{external_services.api_url}/openapi.json", "rel": "service-desc", "type": "application/json"},
            {"href": f"{external_services.api_url}/swagger-ui/", "rel": "service-doc"},
            {"href": f"{external_services.api_url}/api/collections", "rel": "child", "type": "application/json"},
            {"href": f"{external_services.api_url}/api/collections", "rel": "data", "type": "application/json"},
            {"href": f"{external_services.api_url}/api/search", "rel": "search", "type": "application/geo+json"},
            {
                "href": external_services.api_url + "/api/map/{z}/{x}/{y}.mvt",
                "rel": "xyz",
                "title": "Pictures and sequences vector tiles",
                "type": "application/vnd.mapbox-vector-tile",
            },
        ],
        "stac_version": "1.0.0",
        "type": "Catalog",
    }
    # validate the catalog with pystac
    catalog = pystac.Catalog.from_dict(data)
    catalog.validate()


def test_collections_endpoint(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections")
    assert r.status_code == 200
    data = r.json()
    assert len(data["collections"]) == 1

    col = data["collections"][0]
    # validate the collection with pystac
    collection = pystac.Collection.from_dict(col)
    collection.validate()


def get_collections(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections")
    assert r.status_code == 200
    res = []
    for col in r.json()["collections"]:
        c = pystac.Collection.from_dict(col)
        item_link = next(l.href for l in c.links if l.rel == "items")
        items = requests.get(item_link)
        items.raise_for_status()
        for i in items.json()["features"]:
            c.add_item(pystac.Item.from_dict(i))
        res.append(c)
    return res


def test_search_by_id(external_services):
    cols = get_collections(external_services)

    assert len(cols) == 1
    first_col_items = list(cols[0].get_items())
    assert len(first_col_items) == 3

    # post and get queries should yield the same results
    responses = [
        requests.get(f"{external_services.api_url}/api/search?ids={first_col_items[0].id},{first_col_items[1].id}"),
        requests.post(f"{external_services.api_url}/api/search", json={"ids": [first_col_items[0].id, first_col_items[1].id]}),
    ]
    for r in responses:
        assert r.status_code == 200, r.text
        data = r.json()
        assert len(data["features"]) == 2

        assert set([i["id"] for i in data["features"]]) == {first_col_items[0].id, first_col_items[1].id}


def test_search_by_id_not_found(external_services):
    r = requests.get(f"{external_services.api_url}/api/search?ids=00000000-0000-0000-0000-000000000000")
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 0


def test_search_by_bbox_post(external_services):
    cols = get_collections(external_services)

    assert len(cols) == 1
    first_col_items = list(cols[0].get_items())
    assert len(first_col_items) == 3

    # large bbox should get every items
    r = requests.post(f"{external_services.api_url}/api/search", json={"bbox": [-1.684546389, 48.155066389, -1.684468056, 48.155073056]})
    assert r.status_code == 200, r.text
    data = r.json()
    assert len(data["features"]) == 3

    # bbox and limit should work
    r = requests.post(
        f"{external_services.api_url}/api/search", json={"bbox": [-1.684546389, 48.155066389, -1.684468056, 48.155073056], "limit": 1}
    )
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 1

    # reduced bbox should filter only 2 items
    r = requests.post(f"{external_services.api_url}/api/search", json={"bbox": [-1.684506389, 48.155066389, -1.684468056, 48.155071389]})
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 2

    # out of context bbox should return no items
    r = requests.post(f"{external_services.api_url}/api/search", json={"bbox": [10, 40, 11, 41]})
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 0


def test_search_by_bbox_get(external_services):
    cols = get_collections(external_services)

    assert len(cols) == 1
    first_col_items = list(cols[0].get_items())
    assert len(first_col_items) == 3

    # large bbox should get every items
    r = requests.get(f"{external_services.api_url}/api/search?bbox=-1.684546389,48.155066389,-1.684468056,48.155073056")
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 3

    # bbox and limit should work
    r = requests.get(f"{external_services.api_url}/api/search?bbox=-1.684546389,48.155066389,-1.684468056,48.155073056&limit=1")
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 1

    r = requests.get(f"{external_services.api_url}/api/search?bbox=-1.684506389,48.155066389,-1.684468056,48.155071389")
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 2


def test_invalid_search_by_bbox(external_services):
    r = requests.get(f"{external_services.api_url}/api/search?bbox=plop,pouet")
    assert r.status_code == 400
    assert r.text == "Query deserialize error: Invalid parameter: plop is not a valid float"

    r = requests.post(f"{external_services.api_url}/api/search", json={"bbox": ["plop", "pouet"]})
    assert r.status_code == 400
    assert r.text == 'Json deserialize error: invalid type: string "plop", expected f32 at line 1 column 16'

    # bbox should have exactly 4 elements
    r = requests.get(f"{external_services.api_url}/api/search?bbox=-1.684546389,48.155066389,-1.684468056")
    assert r.status_code == 400
    assert r.json() == {"status": 400, "message": "Invalid parameter: Invalid bbox, the boundbing box should be an array with 4 values"}


def test_unknown_pic(external_services):
    r = requests.get(f"{external_services.api_url}/api/pictures/00000000-0000-0000-0000-000000000000/hd.jpg")
    assert r.status_code == 404
    assert r.json() == {"message": "Feature not found", "status": 404}


def test_invalid_uuid_pic(external_services):
    r = requests.get(f"{external_services.api_url}/api/pictures/pouet/hd.jpg")
    assert r.status_code == 404
    assert "UUID parsing failed" in r.text  # at one point, we'll need better error messages


def _get_one_pic_id(external_services):
    one_pic_res = requests.post(f"{external_services.api_url}/api/search", json={"limit": 1})
    assert one_pic_res.status_code == 200
    return one_pic_res.json()["features"][0]["id"]


def test_get_picture_hd_asset(external_services):
    one_pic_id = _get_one_pic_id(external_services)
    r = requests.get(f"{external_services.api_url}/api/pictures/{one_pic_id}/hd.jpg", allow_redirects=False)
    assert r.status_code == 308, r.text
    # this should redirect to the geovisio instance
    assert r.headers["Location"] == f"{external_services.geovisio_instance_1_url}/pictures/{one_pic_id}/hd.jpg"


def test_get_picture_sd_asset(external_services):
    one_pic_id = _get_one_pic_id(external_services)
    r = requests.get(f"{external_services.api_url}/api/pictures/{one_pic_id}/sd.jpg", allow_redirects=False)
    assert r.status_code == 308, r.text
    # this should redirect to the geovisio instance
    assert r.headers["Location"] == f"{external_services.geovisio_instance_1_url}/pictures/{one_pic_id}/sd.jpg"


def test_get_picture_thumb_asset(external_services):
    one_pic_id = _get_one_pic_id(external_services)
    r = requests.get(f"{external_services.api_url}/api/pictures/{one_pic_id}/thumb.jpg", allow_redirects=False)
    assert r.status_code == 308, r.text
    # this should redirect to the geovisio instance
    assert r.headers["Location"] == f"{external_services.geovisio_instance_1_url}/pictures/{one_pic_id}/thumb.jpg"


def test_get_picture_unknown_asset(external_services):
    one_pic_id = _get_one_pic_id(external_services)
    r = requests.get(f"{external_services.api_url}/api/pictures/{one_pic_id}/plop.jpg", allow_redirects=False)
    assert r.status_code == 400
    assert r.json() == {"message": "Invalid parameter: plop is not a valid asset, should be either hd, sd or thumb", "status": 400}


def test_get_picture_hd_asset_bad_format(external_services):
    one_pic_id = _get_one_pic_id(external_services)
    # for the moment only jpeg pic are handled
    r = requests.get(f"{external_services.api_url}/api/pictures/{one_pic_id}/hd.pouet", allow_redirects=False)
    assert r.status_code == 404, r.text
