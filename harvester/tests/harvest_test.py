from .conftest import (
    harvest,
    import_collection_to_geovisio,
    ExternalServicesUrl,
    FIXTURE_DIR,
    cleanup_geovisio_instance,
    add_picture_to_collection,
    wait_until_geovisio_collection_ready,
)
from harvester import Instance, harvest_instances, Config
import pytest
from psycopg.rows import dict_row
import psycopg
import requests
from uuid import UUID


@pytest.fixture(scope="function", autouse=True)
def cleanup_geovisio_instance_1(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers):
    """Delete all collections from geovisio instance 1"""
    cleanup_geovisio_instance(external_services.geovisio_instance_1_url, geovision_instance_1_auth_headers)


@pytest.fixture(scope="function", autouse=True)
def cleanup_geovisio_instance_2(external_services: ExternalServicesUrl, geovision_instance_2_auth_headers):
    """Delete all collections from geovisio instance 2"""
    cleanup_geovisio_instance(external_services.geovisio_instance_2_url, geovision_instance_2_auth_headers)


@pytest.fixture(scope="function", autouse=True)
def cleanup_db_before_test(external_services):
    with psycopg.connect(external_services.db_url) as db:
        db.execute(
            """
        TRUNCATE table collections CASCADE;
        TRUNCATE table harvests CASCADE;
        TRUNCATE table instances CASCADE;
        """
        )


def _get_col_id(col_location: str) -> UUID:
    return UUID(col_location.split("/")[-1])


def test_full_harvest(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers):
    col1 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[FIXTURE_DIR / "col1" / "e1.jpg", FIXTURE_DIR / "col1" / "e2.jpg", FIXTURE_DIR / "col1" / "e3.jpg"],
    )
    col1_id = _get_col_id(col1)

    col2 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 2",
        pic_names=[FIXTURE_DIR / "col2" / "b1.jpg", FIXTURE_DIR / "col2" / "b2.jpg"],
    )
    col2_id = _get_col_id(col2)

    harvest(
        instance=Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url, has_differential_updates=False),
        external_services=external_services,
    )

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT count(*) AS nb_instances FROM instances;").fetchone()
        assert r and r["nb_instances"] == 1
        r = db.execute("SELECT count(*) AS nb_cols FROM collections;").fetchone()
        assert r and r["nb_cols"] == 2
        r = db.execute("SELECT count(*) AS nb_items FROM items;").fetchone()
        assert r and r["nb_items"] == 5

        harvests = db.execute("SELECT id, instance_id, start FROM harvests;").fetchall()
        assert len(harvests) == 1
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert r and len(col_harvests) == 2

    # Harvesting again the instance should not change the harvested items (but this should change the `harvests` and `collection_harvest` that log the harvests)
    harvest(
        instance=Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url),
        external_services=external_services,
    )

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT count(*) AS nb_instances FROM instances;").fetchone()
        assert r and r["nb_instances"] == 1
        r = db.execute("SELECT count(*) AS nb_cols FROM collections;").fetchone()
        assert r and r["nb_cols"] == 2
        r = db.execute("SELECT count(*) AS nb_items FROM items;").fetchone()
        assert r and r["nb_items"] == 5

        harvests = db.execute("SELECT id, instance_id, start FROM harvests ORDER BY start;").fetchall()
        assert len(harvests) == 2

        # again there is only 5 collection_harvest, since the old rows should have been deleted
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            # all collections are marked as 'updated' even if nothing has changed because
            # it's a full harvest and we do not try to detect if there are real changes for the moment
            (col1_id, harvests[-1]["id"]): {"status": "updated"},
            (col2_id, harvests[-1]["id"]): {"status": "updated"},
        }

    # delete 1 collection and add another one to geovisio
    col3 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 3",
        pic_names=[FIXTURE_DIR / "col3" / "1.jpg", FIXTURE_DIR / "col3" / "2.jpg", FIXTURE_DIR / "col3" / "3.jpg"],
    )
    col3_id = _get_col_id(col3)

    delete_col = requests.delete(col2, headers=geovision_instance_1_auth_headers)
    delete_col.raise_for_status()

    # harvest again, we should only have 2 collections now, [col1 and col3]
    harvest(
        instance=Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url),
        external_services=external_services,
    )

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT content->>'title' AS title FROM collections;").fetchall()
        assert r and set(t["title"] for t in r) == {"collection 1", "collection 3"}
        r = db.execute("SELECT count(*) AS nb_items FROM items;").fetchone()
        assert r and r["nb_items"] == 6  # 2*3 items now

        harvests = db.execute("SELECT id, instance_id, start FROM harvests ORDER BY start;").fetchall()
        assert len(harvests) == 3

        # again there is only 5 collection_harvest, since the old rows should have been deleted
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col1_id, harvests[-1]["id"]): {"status": "updated"},
            (col3_id, harvests[-1]["id"]): {"status": "created"},
        }


def test_full_harvest_two_instances(
    external_services: ExternalServicesUrl,
    geovision_instance_1_auth_headers,
    geovision_instance_2_auth_headers,
):
    col1 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[FIXTURE_DIR / "col1" / "e1.jpg", FIXTURE_DIR / "col1" / "e2.jpg", FIXTURE_DIR / "col1" / "e3.jpg"],
    )
    col1_id = _get_col_id(col1)

    col2 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 2",
        pic_names=[FIXTURE_DIR / "col2" / "b1.jpg", FIXTURE_DIR / "col2" / "b2.jpg"],
    )
    col2_id = _get_col_id(col2)

    col3_instance2 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="collection 3 in instance 2",
        pic_names=[FIXTURE_DIR / "col3" / "1.jpg", FIXTURE_DIR / "col3" / "2.jpg", FIXTURE_DIR / "col3" / "2.jpg"],
    )
    col3_id = _get_col_id(col3_instance2)

    instances = [
        Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url, has_differential_updates=False),
        Instance(name="geovisio_instance_2", root_url=external_services.geovisio_instance_2_url, has_differential_updates=False),
    ]
    harvest_instances(db=external_services.db_url, instances=instances, config=Config(host_name=f"{external_services.api_url}/api"))

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT count(*) AS nb_instances FROM instances;").fetchone()
        assert r and r["nb_instances"] == 2
        r = db.execute("SELECT count(*) AS nb_cols FROM collections;").fetchone()
        assert r and r["nb_cols"] == 3
        r = db.execute("SELECT count(*) AS nb_items FROM items;").fetchone()
        assert r and r["nb_items"] == 8

        harvests = db.execute(
            "SELECT h.id AS id, i.name AS instance_name, start FROM harvests h JOIN instances i on h.instance_id = i.id;"
        ).fetchall()
        harvest_on_instance_1 = [h["id"] for h in harvests if h["instance_name"] == "geovisio_instance_1"]
        harvest_on_instance_2 = [h["id"] for h in harvests if h["instance_name"] == "geovisio_instance_2"]
        assert len(harvest_on_instance_1) == 1 and len(harvest_on_instance_2) == 1
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col1_id, harvest_on_instance_1[0]): {"status": "created"},
            (col2_id, harvest_on_instance_1[0]): {"status": "created"},
            (col3_id, harvest_on_instance_2[0]): {"status": "created"},
        }

    # delete col1 and one item from col3 and re-harvest all instances
    requests.delete(col1, headers=geovision_instance_1_auth_headers).raise_for_status()

    pic_to_delete_resp = requests.get(f"{col3_instance2}/items")
    pic_to_delete_resp.raise_for_status()
    pic_to_delete = pic_to_delete_resp.json()["features"][1]["id"]
    requests.delete(f"{col3_instance2}/items/{pic_to_delete}", headers=geovision_instance_2_auth_headers).raise_for_status()

    harvest_instances(db=external_services.db_url, instances=instances, config=Config(host_name=f"{external_services.api_url}/api"))

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT count(*) AS nb_instances FROM instances;").fetchone()
        assert r and r["nb_instances"] == 2
        r = db.execute("SELECT content->>'title' AS title FROM collections;").fetchall()
        assert r and set(t["title"] for t in r) == {"collection 2", "collection 3 in instance 2"}

        r = db.execute("SELECT collection_id, count(*) AS nb_items FROM items GROUP BY collection_id;").fetchall()
        assert r and {t["collection_id"]: t["nb_items"] for t in r} == {col3_id: 2, col2_id: 2}

        harvests = db.execute(
            "SELECT h.id AS id, i.name AS instance_name, start FROM harvests h JOIN instances i on h.instance_id = i.id;"
        ).fetchall()
        harvest_on_instance_1 = [h["id"] for h in harvests if h["instance_name"] == "geovisio_instance_1"]
        harvest_on_instance_2 = [h["id"] for h in harvests if h["instance_name"] == "geovisio_instance_2"]
        assert len(harvest_on_instance_1) == 2 and len(harvest_on_instance_2) == 2
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col2_id, harvest_on_instance_1[-1]): {"status": "updated"},
            (col3_id, harvest_on_instance_2[-1]): {"status": "updated"},
        }


def test_differential_harvest(
    external_services: ExternalServicesUrl,
    geovision_instance_1_auth_headers,
):
    """
    Harvest several times an instance supporting differential harvest.
    After the first harvest, each harvest will only collect collections that have been created/updated/deleted since last harvest
    """
    col1 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[FIXTURE_DIR / "col1" / "e1.jpg", FIXTURE_DIR / "col1" / "e2.jpg", FIXTURE_DIR / "col1" / "e3.jpg"],
    )
    col1_id = _get_col_id(col1)

    col2 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 2",
        pic_names=[FIXTURE_DIR / "col2" / "b1.jpg", FIXTURE_DIR / "col2" / "b2.jpg"],
    )
    col2_id = _get_col_id(col2)

    instance = Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url, has_differential_updates=True)
    harvest(instance=instance, external_services=external_services)

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT name FROM instances;").fetchall()
        assert r and r == [{"name": "geovisio_instance_1"}]
        r = db.execute("SELECT count(*) AS nb_cols FROM collections;").fetchone()
        assert r and r["nb_cols"] == 2
        r = db.execute("SELECT count(*) AS nb_items FROM items;").fetchone()
        assert r and r["nb_items"] == 5

        harvests = db.execute("SELECT id, instance_id, start FROM harvests;").fetchall()
        assert len(harvests) == 1
        first_harvest = harvests[0]
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert r and len(col_harvests) == 2
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col1_id, first_harvest["id"]): {"status": "created"},
            (col2_id, first_harvest["id"]): {"status": "created"},
        }

    # Harvesting again the instance should not change the harvested items (but this should change the `harvests` and `collection_harvest` that log the harvests)
    harvest(instance=instance, external_services=external_services)

    # since nothing has changed in the crawled instances, nothing should have changed in the database,
    # apart from a new harvest being created (but without `collection_harvest` attached)
    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT name FROM instances;").fetchall()
        assert r and r == [{"name": "geovisio_instance_1"}]
        r = db.execute("SELECT count(*) AS nb_cols FROM collections;").fetchone()
        assert r and r["nb_cols"] == 2
        r = db.execute("SELECT count(*) AS nb_items FROM items;").fetchone()
        assert r and r["nb_items"] == 5

        harvests = db.execute("SELECT id, instance_id, start FROM harvests ORDER BY start;").fetchall()
        assert len(harvests) == 2
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col1_id, first_harvest["id"]): {"status": "created"},
            (col2_id, first_harvest["id"]): {"status": "created"},
        }

    # delete 1 collection and add another one to geovisio
    col3 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 3",
        pic_names=[FIXTURE_DIR / "col3" / "1.jpg", FIXTURE_DIR / "col3" / "2.jpg"],
    )
    col3_id = _get_col_id(col3)

    requests.delete(col2, headers=geovision_instance_1_auth_headers).raise_for_status()

    # harvest again, we should only have 2 collections now, [col1 and col3]
    harvest(instance=instance, external_services=external_services)

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT content->>'title' AS title FROM collections;").fetchall()
        assert r and set(t["title"] for t in r) == {"collection 1", "collection 3"}
        r = db.execute("SELECT collection_id, count(*) AS nb_items FROM items GROUP BY collection_id;").fetchall()
        assert r and {t["collection_id"]: t["nb_items"] for t in r} == {col1_id: 3, col3_id: 2}

        harvests = db.execute("SELECT id, instance_id, start FROM harvests ORDER BY start;").fetchall()
        assert len(harvests) == 3

        # for differential harvest, we keep the history of the collection_harvest
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col1_id, harvests[0]["id"]): {"status": "created"},
            (col3_id, harvests[-1]["id"]): {"status": "created"},
        }

    # update one collection by adding a picture to it
    add_picture_to_collection(col3, pic=FIXTURE_DIR / "col3" / "3.jpg", position=3)

    wait_until_geovisio_collection_ready(col3, wanted_state={"sequence_status": "ready", "items": {i: "ready" for i in range(1, 4)}})

    harvest(instance=instance, external_services=external_services)

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT content->>'title' AS title FROM collections;").fetchall()
        assert r and set(t["title"] for t in r) == {"collection 1", "collection 3"}
        r = db.execute("SELECT collection_id, count(*) AS nb_items FROM items GROUP BY collection_id;").fetchall()
        assert r and {t["collection_id"]: t["nb_items"] for t in r} == {col1_id: 3, col3_id: 3}

        harvests = db.execute("SELECT id, instance_id, start FROM harvests ORDER BY start;").fetchall()
        assert len(harvests) == 4

        # for differential harvest, we keep the history of the collection_harvest
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col1_id, harvests[0]["id"]): {"status": "created"},
            (col3_id, harvests[-2]["id"]): {"status": "created"},
            (col3_id, harvests[-1]["id"]): {"status": "updated"},
        }
