# Migrations for the meta catalog

Most migrations are handled by [PGStac](https://stac-utils.github.io/pgstac/), but for custom migrations outside of it, we use [yoyo-migrations](https://ollycope.com/software/yoyo/latest/#).

## Preparing the database

Before applying the migrations, a database must be provided.

As the admin of postgres, you can do those psql commands for this:

- Create a PostgreSQL role `geovisio` with this command:

```bash
sudo su - postgres -c "psql -c \"CREATE ROLE panoramax LOGIN PASSWORD 'some_very_secret_key'\""
```

- Create a new database (with __UTF-8 encoding__) using this command:

```bash
sudo su - postgres -c "psql -c \"CREATE DATABASE panoramax ENCODING 'UTF-8' TEMPLATE template0 OWNER panoramax\""
```

- Enable PostGIS and UUID extensions in your database:

```bash
sudo su - postgres -c 'psql -d panoramax -c "CREATE EXTENSION \"postgis\""'
sudo su - postgres -c 'psql -d panoramax -c "CREATE EXTENSION \"uuid-ossp\""'
```
## Apply the migrations

```bash
yoyo apply --batch --database postgresql+psycopg://<connection_url>
```

## Create a new migration

```bash
yoyo new --sql -m "<description of the migration>"
```