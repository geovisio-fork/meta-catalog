-- instances
-- depends: 

-- need CREATE EXTENSION "uuid-ossp";
CREATE EXTENSION IF NOT EXISTS "postgis";
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE OR REPLACE FUNCTION compute_picture_type(content jsonb)
RETURNS VARCHAR AS $$
    SELECT
        CASE 
            WHEN
            content #>> '{properties,pers:interior_orientation,field_of_view}' = '360' 
            THEN 
                'equirectangular' 
            ELSE 'flat'
        END
    ;
$$ LANGUAGE SQL IMMUTABLE STRICT;

CREATE TABLE IF NOT EXISTS collections (
    id UUID PRIMARY KEY GENERATED ALWAYS AS ((content ->> 'id')::UUID) STORED,
    content JSONB NOT NULL,

	computed_picture_type VARCHAR,
	computed_model VARCHAR,
	computed_capture_date DATE,
	computed_geom GEOMETRY(MultiLineString, 4326)
);

CREATE INDEX collections_geometry_idx ON collections USING GIST(computed_geom);

-- The 'items' table contain raw JSON item content crawl from panoramax instances
-- All fields are derived from the raw json.
CREATE TABLE IF NOT EXISTS items (
    id UUID PRIMARY KEY GENERATED ALWAYS AS ((content ->> 'id')::UUID) STORED,
    content JSONB NOT NULL,
	collection_id UUID NOT NULL REFERENCES collections(id) GENERATED ALWAYS AS ((content ->> 'collection')::UUID) STORED,
	picture_type VARCHAR NOT NULL GENERATED ALWAYS AS (compute_picture_type(content)) STORED,

	geometry geometry NOT NULL GENERATED ALWAYS AS (ST_GeomFromGeoJSON(content -> 'geometry')) STORED,
    datetime TIMESTAMPTZ NOT NULL -- datetime cannot be a generated column since datetime parsing postresql functions are not immutable
);

CREATE INDEX items_collection_id_idx ON items(collection_id);
CREATE INDEX items_geometry_idx ON items USING GIST(geometry);
CREATE INDEX items_datetime_idx ON items(datetime);

CREATE TABLE  IF NOT EXISTS instances(
	id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    name VARCHAR NOT NULL,
    url VARCHAR NOT NULL,

	CONSTRAINT name_unique UNIQUE (name) 
);

ALTER TABLE collections ADD COLUMN IF NOT EXISTS instance_id UUID;
ALTER TABLE collections ADD CONSTRAINT instance_fk_id FOREIGN KEY (instance_id) REFERENCES instances (id);

CREATE TABLE  IF NOT EXISTS harvests(
	id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    instance_id UUID NOT NULL REFERENCES instances(id),
	start TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TYPE harvested_collection_status AS ENUM (
	'created', -- When the collection is harvested for the first time
	'updated', -- When the collection has been updated
	'deleted' -- When the collection has been deleted
);

CREATE TABLE  IF NOT EXISTS collection_harvest(
    collection_id UUID NOT NULL REFERENCES collections(id),
    harvest_id UUID NOT NULL REFERENCES harvests(id),
	start TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    status harvested_collection_status NOT NULL
);


-- Function to be called when all items of a collection have been inserted, to compute all needed fields on this collection
DROP FUNCTION IF EXISTS compute_collections_fields CASCADE;
CREATE FUNCTION compute_collections_fields(col_id uuid) RETURNS text AS $$
BEGIN
	WITH
    collection_geom AS (
        SELECT id, ST_Multi(ST_MakeLine(
            ARRAY(
                SELECT
                    i.geometry
                FROM items i
                WHERE i.collection_id = collections.id
                ORDER BY
                    i.datetime
                )
            )) AS geom
        FROM collections
        WHERE collections.id = col_id
    ),
    aggregated_items AS (
        SELECT
            collection_id,
            MIN(datetime::DATE) AS day,
            ARRAY_AGG(DISTINCT TRIM(CONCAT(content #>> '{properties,pers:interior_orientation,camera_manufacturer}', ' ', content #>> '{properties,pers:interior_orientation,camera_model}'))) AS models,
            ARRAY_AGG(DISTINCT compute_picture_type(content)) AS picture_type
            FROM items
        WHERE collection_id = col_id
        GROUP BY collection_id
    )
    UPDATE collections
    SET
        computed_geom = collection_geom.geom,
        computed_picture_type = CASE WHEN array_length(aggregated_items.picture_type, 1) = 1 THEN aggregated_items.picture_type[1] ELSE NULL END,
        computed_model = CASE WHEN array_length(aggregated_items.models, 1) = 1 THEN aggregated_items.models[1] ELSE NULL END,
        computed_capture_date = aggregated_items.day
    FROM collection_geom
    JOIN aggregated_items on aggregated_items.collection_id = collection_geom.id
    WHERE collections.id = col_id AND col_id = aggregated_items.collection_id;
RETURN col_id;
END $$ LANGUAGE plpgsql;
