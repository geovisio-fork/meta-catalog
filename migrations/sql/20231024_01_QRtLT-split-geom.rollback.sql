-- split_geom
-- depends: 20230925_01_2lgbu-instances

ALTER TABLE items RENAME COLUMN geom TO geometry;

-- put back the function from the initial migration
DROP FUNCTION IF EXISTS compute_collections_fields CASCADE;
CREATE FUNCTION compute_collections_fields(col_id uuid) RETURNS text AS $$
BEGIN
	WITH
    collection_geom AS (
        SELECT id, ST_Multi(ST_MakeLine(
            ARRAY(
                SELECT
                    i.geometry
                FROM items i
                WHERE i.collection_id = collections.id
                ORDER BY
                    i.datetime
                )
            )) AS geom
        FROM collections
        WHERE collections.id = col_id
    ),
    aggregated_items AS (
        SELECT
            collection_id,
            MIN(datetime::DATE) AS day,
            ARRAY_AGG(DISTINCT TRIM(CONCAT(content #>> '{properties,pers:interior_orientation,camera_manufacturer}', ' ', content #>> '{properties,pers:interior_orientation,camera_model}'))) AS models,
            ARRAY_AGG(DISTINCT compute_picture_type(content)) AS picture_type
            FROM items
        WHERE collection_id = col_id
        GROUP BY collection_id
    )
    UPDATE collections
    SET
        computed_geom = collection_geom.geom,
        computed_picture_type = CASE WHEN array_length(aggregated_items.picture_type, 1) = 1 THEN aggregated_items.picture_type[1] ELSE NULL END,
        computed_model = CASE WHEN array_length(aggregated_items.models, 1) = 1 THEN aggregated_items.models[1] ELSE NULL END,
        computed_capture_date = aggregated_items.day
    FROM collection_geom
    JOIN aggregated_items on aggregated_items.collection_id = collection_geom.id
    WHERE collections.id = col_id AND col_id = aggregated_items.collection_id;
RETURN col_id;
END $$ LANGUAGE plpgsql;
