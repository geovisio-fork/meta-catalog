-- split_geom
-- depends: 20230925_01_2lgbu-instances

-- rename the geometry from `geometry` to `geom` not to conflict with the type
ALTER TABLE items RENAME COLUMN geometry TO geom;

-- Update compute_collections_fields, to split collection geometry in several linestrings if the points are too far appart.
-- Note: The collection will not have a `computed_geom` if the geometry is not a multilinestring,
-- for example if there are only 1 item (the geometry would be a POINT),
-- or only pictures too far appart (the geometry would be a GEOMETRYCOLLECTION of POINTS)
DROP FUNCTION IF EXISTS compute_collections_fields CASCADE;
CREATE FUNCTION compute_collections_fields(col_id uuid) RETURNS text AS $$
BEGIN
	WITH
    all_items AS (
        SELECT
        i.geom AS geom, i.collection_id as collection_id
        FROM items i
        where collection_id = col_id
        ORDER BY
        i.datetime
    )
    , pic_line AS (
            SELECT collection_id, ST_MakeLine(geom) AS geom
            FROM all_items GROUP BY collection_id
    )
    , segments as (
            select
            collection_id,
            (st_dumpsegments(geom)).geom as segment
            from pic_line
    )
    -- make a multiline geometry for the collection, spliting each items separated by more than 50 meters
    , collection_geom as (
            select
            collection_id as id,
            st_linemerge(st_collect(segment)) as geom
            from segments
            where st_length(segment::geography) < 50
            group by collection_id
    ) 
    , collection_multi_ls_geom as (
            select id,
            st_multi(geom) as geom
            from collection_geom
            where not ST_IsEmpty(geom)
    )
    , aggregated_items AS (
        SELECT
            collection_id,
            MIN(datetime::DATE) AS day,
            ARRAY_AGG(DISTINCT TRIM(CONCAT(content #>> '{properties,pers:interior_orientation,camera_manufacturer}', ' ', content #>> '{properties,pers:interior_orientation,camera_model}'))) AS models,
            ARRAY_AGG(DISTINCT compute_picture_type(content)) AS picture_type
            FROM items
        WHERE collection_id = collection_id
        GROUP BY collection_id
    )
    UPDATE collections
    SET
        computed_geom = collection_multi_ls_geom.geom,
        computed_picture_type = CASE WHEN array_length(aggregated_items.picture_type, 1) = 1 THEN aggregated_items.picture_type[1] ELSE NULL END,
        computed_model = CASE WHEN array_length(aggregated_items.models, 1) = 1 THEN aggregated_items.models[1] ELSE NULL END,
        computed_capture_date = aggregated_items.day
    FROM collection_multi_ls_geom
    JOIN aggregated_items on aggregated_items.collection_id = collection_multi_ls_geom.id
    WHERE collections.id = col_id AND col_id = aggregated_items.collection_id;
RETURN col_id;
END $$ LANGUAGE plpgsql;

